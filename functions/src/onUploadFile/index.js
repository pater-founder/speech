'use strict';
const functions = require('firebase-functions');
const path = require('path');
const { db , bucket } = require('../firebaseServices');
const { generateMonoAudio } = require('./generateMonoAudio');
const { registerAudio } = require('./registerAudio');

exports.onUploadFile = functions.runWith({ memory: '2GB', timeoutSeconds: 450 }).storage.object().onFinalize(async (object) => {
    if (!object.contentType.startsWith('audio/')) {
        console.log('This is not an audio.');
        return null;
    }
    const fileName = path.basename(object.name);
    if (fileName.endsWith('_monoOutput.flac')) {
        console.log('Already a converted audio.');
        return null;
    }
    const metadata = await generateMonoAudio(object, bucket());
    return registerAudio(metadata, db());
});
