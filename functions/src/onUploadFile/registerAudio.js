const path = require('path');

exports.registerAudio = async (metadata, db) => {
    const fileName = path.basename(metadata.sourceFileMetadata.name);
    const ownerUid = metadata.sourceFileMetadata.metadata.ownerUid;

    const uploadedAudioRef = db.collection('accounts').doc(ownerUid).collection('uploaded-audio');
    return uploadedAudioRef.doc(fileName).set({
        fileName,
        ownerUid,
        status: 'new',
        size: metadata.sourceFileMetadata.size,
        duration: metadata.audioMetadata.duration,
        timeCreated: metadata.sourceFileMetadata.timeCreated,
        ...metadata
    });
};
