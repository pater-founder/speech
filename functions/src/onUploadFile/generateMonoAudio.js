const path = require('path');
const os = require('os');
const fs = require('fs');
const ffmpeg = require('fluent-ffmpeg');
const ffmpeg_static = require('ffmpeg-static');
const ffprobe_static = require('ffprobe-static');

// Makes an ffmpeg command return a promise.
function promisifyCommand(command) {
    return new Promise((resolve, reject) => {
        command.on('end', resolve).on('error', reject).run();
    });
}

exports.generateMonoAudio = async (object, bucket) => {
    const filePath = object.name; // File path in the bucket.
    const fileName = path.basename(filePath);

    // Download file from bucket.
    const tempFilePath = path.join(os.tmpdir(), fileName);
    // We add a '_monoOutput.flac' suffix to target audio file name. That's where we'll upload the converted audio.
    const targetTempFileName = fileName.replace(/\.[^/.]+$/, '') + '_monoOutput.flac';
    const targetTempFilePath = path.join(os.tmpdir(), targetTempFileName);
    const targetStorageFilePath = path.join(path.dirname(filePath), targetTempFileName);

    await bucket.file(filePath).download({destination: tempFilePath});
    console.log('Audio downloaded locally to', tempFilePath);

    const ffmp = ffmpeg(tempFilePath);
    // Convert the audio to mono channel using FFMPEG.
    let command = ffmp
        .setFfmpegPath(ffmpeg_static.path)
        .audioChannels(1)
        .format('flac')
        .output(targetTempFilePath);

    await promisifyCommand(command);
    console.log('Output audio created at', targetTempFilePath);

    let audioMetadata = {};
    ffmp
        .setFfprobePath(ffprobe_static.path)
        .ffprobe(function(err, metadata) {
            if (err) {
                return console.error('ffprobe-error:', err);
            }
            audioMetadata.duration = metadata.format.duration;
            audioMetadata.formatName = metadata.format.format_name;
            audioMetadata.bitRate = metadata.format.bit_rate;
            audioMetadata.streamsCount = metadata.streams.length;
            audioMetadata.sampleRateHertz = metadata.streams[0].sample_rate;
            audioMetadata.codecName = metadata.streams[0].codec_name;
            audioMetadata.channels = metadata.streams[0].channels;
        });

    // Uploading the audio.
    const [ monoObject ] = await bucket.upload(targetTempFilePath, {destination: targetStorageFilePath, resumable: false });
    console.log('Output audio uploaded to', targetStorageFilePath);

    [ { name, size } ] = await monoObject.getMetadata();
    const monoFileMetadata = { name, size };

    // Once the audio has been uploaded delete the local file to free up disk space.
    fs.unlinkSync(tempFilePath);
    fs.unlinkSync(targetTempFilePath);
    console.log('Temporary files removed.', targetTempFilePath);

    return {
        sourceFileMetadata: object,
        monoFileMetadata,
        audioMetadata
    };
};
