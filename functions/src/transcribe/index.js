'use strict';
const functions = require('firebase-functions');

const { db  } = require('../firebaseServices');

const speech = require('@google-cloud/speech');
const client = new speech.SpeechClient();


exports.transcribe = functions.https.onCall(async (data, context) => {
    const uid = context.auth.uid;
    const userDocRef = db().collection('accounts').doc(uid);
    const newFilesSnap = await userDocRef
        .collection('uploaded-audio')
        .where('status', '==', 'new')
        .get();

    newFilesSnap.forEach(async fileDoc => {
        const fileMetadata = fileDoc.data();
        const request = {
            config: {
                encoding: 'FLAC',
                sampleRateHertz: fileMetadata.audioMetadata.sampleRateHertz,
                enableWordTimeOffsets: true
            },
            audio: {
                uri: `gs://${fileMetadata.sourceFileMetadata.bucket}/${fileMetadata.monoFileMetadata.name}`
            },
        };
        console.log('request:', request);
        if (fileMetadata.duration < 60) {
            fileDoc.ref.update({ status: 'processing', timeProcessingStarted: new Date().toISOString() });
            const [ response ] = await client.recognize(request);
            await userDocRef.collection('transcriptions').doc(fileMetadata.fileName).set({
                fileName: fileMetadata.fileName,
                ownerUid: uid,
                results: response.results,
                timeCreated: new Date().toISOString(),
                recognizeType: 'sync'
            });
            fileDoc.ref.update({ status: 'done', timeProcessingDone: new Date().toISOString() });
        } else {
            const [ operation ] = await client.longRunningRecognize(request);
            fileDoc.ref.update({
                status: 'processing',
                timeProcessingStarted: new Date().toISOString(),
                operationName: operation.latestResponse.name
            });
        }
    });

    return console.log('transcribe success');
});
