'use strict';
const admin = require('firebase-admin');

admin.initializeApp();

let dbClient;
const db = () => {
    dbClient = dbClient || admin.firestore();
    return dbClient;
};
exports.db = db;

let bucketClient;
const bucket = () => {
    bucketClient = bucketClient || admin.storage().bucket();
    return bucketClient;
};
exports.bucket = bucket;
