'use strict';
const functions = require('firebase-functions');
const { auth } = require('google-auth-library');
const { isEmpty } = require('lodash');

const { db  } = require('../firebaseServices');

let client;

exports.pollRecognitionResult = functions.https.onCall(async (data, context) => {
    const uid = context.auth.uid;
    const operations = data.operations;

    if (!isEmpty(operations)) {
        client = client || await auth.getClient();

        operations.forEach(async ({ fileName, operationName}) => {
            const res = await client.request({ url: `https://speech.googleapis.com/v1/operations/${operationName}` });
            console.log('proverka operation res:', res);
            if (res.data.done) {
                const userDocRef = db().collection('accounts').doc(uid);
                await userDocRef.collection('transcriptions').doc(fileName).set({
                    fileName,
                    ownerUid: uid,
                    results: res.data.response.results,
                    timeCreated: new Date().toISOString(),
                    recognizeType: 'async'
                });
                userDocRef.collection('uploaded-audio').doc(fileName).update({
                    status: 'done',
                    timeProcessingDone: new Date().toISOString(),
                    timeOperationDone: res.data.metadata.lastUpdateTime
                });
            }
        });
    }

    return;
});
