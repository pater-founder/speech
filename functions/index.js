const { onUploadFile } = require('./src/onUploadFile');
const { transcribe } = require('./src/transcribe');
const { pollRecognitionResult } = require('./src/pollRecognitionResult');

exports.onUploadFile = onUploadFile;

exports.transcribe = transcribe;

exports.pollRecognitionResult = pollRecognitionResult;
