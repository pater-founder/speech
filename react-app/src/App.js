import React, { Component } from 'react';
import './theme/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

import './App.css';

import { BrowserRouter as Router, Route } from 'react-router-dom';
import Main from './pages/Main';
import Account from './pages/Account';
import Test from './pages/Test';

class App extends Component {
  render() {
    return (
        <Router>
            <div>
                <Route exact path="/" component={Main}/>
                <Route path="/account" component={Account}/>
                <Route path="/test" component={Test}/>
            </div>
        </Router>
    );
  }
}

export default App;
