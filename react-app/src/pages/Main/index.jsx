import React, { Component } from 'react';
import s from './main.module.css';

import Login from 'containers/Login';

class Main extends Component {
    state = { showLogin: false };
    render() {
        return (
            <div className={s['Main']}>
                <div className={s['MainContainer']}>
                    <div className={s['MainGradientContainer']}>
                        <div className={s['MainContent']}>
                            <div className={s['MainTitle']}>
                                Upload audio and get speech to text transcription
                                {/*Переводите аудиозаписи в текст*/}
                            </div>
                            <div className={s['MainSubscr']}>
                                Automatic speech recognition in 120 languages. Karaoke-style editor to polish result
                                {/*Технологии искусственного интеллекта позволяют автоматически переводить аудиозаписи на 120 языках в текст. А удобный редактор поможет в караоке-стиле довести результат до идеального*/}
                            </div>
                            <div className={s['MainCallToAction']}>
                                <a className={s['MainCallToActionButton']} onClick={() => this.setState({ showLogin: true })}>
                                    <table style={{ width: '100%', height: '100%'}}>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    Try free
                                                    {/*Попробовать бесплатно*/}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={s['SecondScreen']}>
                    <div className={s['SecondScreenContent']}>
                        <div className={s['SecondScreenTitle']}>
                            We created this service to help you make your transcribation easy and fast as possible
                            {/*Мы создали сервис, чтобы помочь вам делать транскрибацию аудиозаписей просто и быстро*/}
                        </div>
                        <div className={s['SecondScreenSubscr']}>
                            This is a great way to avoid routine manual transcription
                            {/*Это отличный способ избежать рутинную ручную транскрибацию.*/}
                        </div>
                        <div className={s['SecondScreenTable']}>
                            <div className={s['SecondScreenTableItem']}>
                                <i className="pi pi-clock" style={{ fontSize: '100px', marginBottom: '25px', color: 'cadetBlue' }}></i>
                                <div className={s['SecondScreenTextWrapper']}>
                                    <div className={s['SecondScreenTextHeader']}>
                                        Time
                                        {/*Время*/}
                                    </div>
                                    <div className={s['SecondScreenTextDescr']}>
                                        Upload file and get result in half-time of audio duration
                                        {/*Загрузите аудиофайл и получите результат в течение половины длительности записи*/}
                                    </div>
                                </div>
                            </div>
                            <div className={s['SecondScreenTableItem']}>
                                <i className="pi pi-money-bill" style={{ fontSize: '100px', marginBottom: '25px', color: 'cadetBlue' }}></i>
                                <div className={s['SecondScreenTextWrapper']}>
                                    <div className={s['SecondScreenTextHeader']}>
                                        Cost
                                        {/*Экономия*/}
                                    </div>
                                    <div className={s['SecondScreenTextDescr']}>
                                        The cost of transcription is much cheaper than manual transcription. 8¢/min
                                        {/*Стоимость транскрибации значительно дешевле услуги ручной транскрибации. 6 руб./мин.*/}
                                    </div>
                                </div>
                            </div>
                            <div className={s['SecondScreenTableItem']}>
                                <i className="pi pi-chart-line" style={{ fontSize: '100px', marginBottom: '25px', color: 'cadetBlue' }}></i>
                                <div className={s['SecondScreenTextWrapper']}>
                                    <div className={s['SecondScreenTextHeader']}>
                                        Efficiency
                                        {/*Эффективность*/}
                                    </div>
                                    <div className={s['SecondScreenTextDescr']}>
                                        No need to negotiate with contractors. Just download recording and use the convenient karaoke-style editor
                                        {/*Не нужно договариваться с подрядчиками. Просто загрузите запись и воспользуйтесь удобным редактором в караоке-стиле*/}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.showLogin && <Login />}
            </div>
        );
    }
}

export default Main;
