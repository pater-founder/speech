import React, { Component } from 'react';
import { Link, Switch, Route } from 'react-router-dom';
import { COST_PER_MINUTE } from "constants/money";
import Editor from 'containers/CodexEditor';
import UploadButton from 'containers/UploadButton';
import StartTranscribationButton from 'containers/StartTranscribationButton';
import PaypalButton from 'containers/PaypalButton';
import { durationToTime } from 'utils/time'

import s from './account.module.css';
import { Button } from 'primereact/button';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { ProgressBar } from 'primereact/progressbar';
import { Dialog } from "primereact/dialog";

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import "react-tabs/style/react-tabs.css";

import isEmpty from 'lodash/isEmpty';
import floor from 'lodash/floor'
import round from 'lodash/round';
import differenceBy from 'lodash/differenceBy';
import intersectionBy from 'lodash/intersectionBy';
import clamp from 'lodash/clamp';

import addSeconds from 'date-fns/addSeconds';
import isAfter from 'date-fns/isAfter';
import differenceInSeconds from 'date-fns/differenceInSeconds';

const storage = window.firebase.storage();
const db = window.firebase.firestore();
const auth = window.firebase.auth();
const functions = window.firebase.app().functions();
const transcribe = functions.httpsCallable('transcribe');
const pollRecognitionResult = functions.httpsCallable('pollRecognitionResult');
const storageRoot = storage.ref();

class Account extends Component {
    state = {
        user: {
            email: null,
            uid: null
        },
        uploadedAudio: [],
        processingAudio: [],
        finishedAudio: [],
        deletionDialogVisible: false,
        deletionFileName: null,
        checkoutDialogVisible: false,
        checkoutFileName: null,
        checkoutDuration: null
    };
    pollingIntervalId = null;
    progressIntervalId = null;

    onTranscriptButtonClick = async () => {
        await transcribe();
        return console.log('transcribe success');
    };

    startPolling = () => {
        if (!this.pollingIntervalId) {
            this.pollingIntervalId = window.setInterval(() => {
                const filesToPoll = this.state.processingAudio.filter(({ duration, timeProcessingStarted, operationName }) => {
                    if (!operationName)
                        return false;
                    return isAfter(new Date(), addSeconds(new Date(timeProcessingStarted), duration * 0.25));

                });
                if (!isEmpty(filesToPoll)) {
                    const operations = filesToPoll.map(({ fileName, operationName }) => ({ fileName, operationName }));
                    pollRecognitionResult({ operations });
                }
            }, 5000)
        }
    }
    stopPolling = () => {
        if (this.pollingIntervalId) {
            window.clearInterval(this.pollingIntervalId);
            this.pollingIntervalId = null;
        }
    }
    startProgress = () => {
        if (!this.progressIntervalId) {
            this.progressIntervalId = window.setInterval(() => {
                this.setState(
                    state => ({
                        processingAudio: state.processingAudio.map(
                            ({ progress = 0, duration, ...rest }) => ({ progress: progress + ((100 / duration) * 2), duration, ...rest }))
                    })
                );
            }, 1000)
        }
    }
    stopProgress = () => {
        if (this.progressIntervalId) {
            window.clearInterval(this.progressIntervalId);
            this.progressIntervalId = null;
        }
    }

    componentDidMount() {
        auth.onAuthStateChanged(async user => {
            this.setState({ user });
            const userRef = db.collection('accounts').doc(user.uid);
            await userRef.set({ uid: user.uid, email: user.email });
            console.log('acc success');
            const allFilesRef = userRef.collection('uploaded-audio');
            allFilesRef.where('status', '==', 'new').onSnapshot(snap => {
                this.setState({ uploadedAudio: snap.docs.map(d => d.data()) })
            });
            allFilesRef.where('status', '==', 'processing').onSnapshot(snap => {
                const processingAudioOriginal = snap.docs.map(d => d.data());
                const processingAudioWithEstimatedFinish = processingAudioOriginal.map( // добавляется расчётное время финиша и расчётный прогресс
                    file => ({
                        timeEstimatedFinish: addSeconds(new Date(file.timeProcessingStarted), file.duration * 0.5).toISOString(),
                        progress: clamp(((100 / file.duration) * 2) * differenceInSeconds(new Date(), new Date(file.timeProcessingStarted)), 0, 99.9),
                        ...file
                    })
                );
                this.setState(state => { // интерсекшн ради сохранения прогресса
                    const intersec = intersectionBy(state.processingAudio, processingAudioWithEstimatedFinish, 'filrName' );
                    const diff = differenceBy(processingAudioWithEstimatedFinish, state.processingAudio, 'fileName');
                    return {
                        processingAudio: intersec.concat(diff)
                    }
                });
                if (!isEmpty(processingAudioOriginal)) {
                    this.startPolling();
                    this.startProgress();
                } else {
                    this.stopPolling();
                    this.stopProgress();
                }
            });
            allFilesRef.where('status', '==', 'done').onSnapshot(snap => {
                this.setState({ finishedAudio: snap.docs.map(d => d.data()) })
            });
        });
        console.log('auth;', auth);
    }

    componentWillUnmount() {
        window.clearInterval(this.pollingIntervalId);
        window.clearInterval(this.progressIntervalId);
    }

    nameLinkCell = ({ fileName }) => {
        return <Link to={`${this.props.match.url}/editor/${encodeURI(fileName)}`}>{fileName}</Link>
        // return <a onClick={() => this.goToTranscription(fileName)}>{fileName}</a>
    };
    durationCell = ({ duration }) => {
        return <span>{durationToTime(duration)}</span>
    }
    deleteCell = ({ fileName }) => {
        return <i
            onClick={e => this.setState({ deletionDialogVisible: true, deletionFileName: fileName })}
            className="pi pi-trash"
        >
        </i>
    }
    transcribeCell = ({ fileName, duration }) => {
        return <Button
            onClick={e => this.setState({
                checkoutDialogVisible: true,
                checkoutFileName: fileName,
                checkoutDuration: duration
            })}
            label="transcribe"
            className="p-button-rounded"
        >
        </Button>
    }
    costCell = ({ duration }) => {
        return round((duration / 60) * COST_PER_MINUTE, 2) + '$'
    }
    progressCell = ({ progress }) => {
        return <ProgressBar value={clamp(floor(progress, 1), 99.9)} />
    }
    timeCreatedCell = ({ timeCreated }) => {
        return new Date(timeCreated).toLocaleString();
    }
    timeEstimatedFinishCell = ({ timeEstimatedFinish }) => {
        return new Date(timeEstimatedFinish).toLocaleTimeString();
    }

    renderDeletionDialog = () => {
        const onHide = () => this.setState({deletionDialogVisible: false, deletionFileName: null });
        const onDelete = async () => {
           await db
                .collection('accounts')
                .doc(this.state.user.uid)
                .collection('uploaded-audio')
                .doc(this.state.deletionFileName)
                .delete();
           console.log(`file ${this.state.deletionFileName} has been deleted`);
           onHide();
        };
        const footer = (
            <div>
                <Button label="Cancel" icon="pi pi-times" onClick={onHide} />
                <Button label="Ok" icon="pi pi-check" onClick={onDelete} />
            </div>
        );
        return (
            <Dialog
                header={`Delete ${this.state.deletionFileName}?`}
                visible={this.state.deletionDialogVisible}
                style={{width: '30vw'}}
                modal={true}
                onHide={onHide}
                footer={footer}
            >
            </Dialog>
        )
    }

    renderCheckoutDialog = () => {
        const onHide = () => this.setState({
            checkoutDialogVisible: false,
            checkoutFileName: null,
            checkoutDuration: null
        });
        const amount = round((this.state.checkoutDuration / 60) * COST_PER_MINUTE, 2);
        return (
            <Dialog
                header={`Checkout ${this.state.checkoutFileName}`}
                visible={this.state.checkoutDialogVisible}
                style={{width: '30vw'}}
                modal={true}
                onHide={onHide}
            >
                <table style={{ marginBottom: '6px' }}>
                    <tbody>
                        <tr style={{ height: ' 24px' }}>
                            <td style={{ width: '200px' }}>Duration</td>
                            <td>{`${round(this.state.checkoutDuration / 60, 2)} min`}</td>
                        </tr>
                        <tr style={{ height: ' 24px' }}>
                            <td>Price</td>
                            <td>8¢/min</td>
                        </tr>
                        <tr style={{ fontWeight: 'bold'}}>
                            <td>Total</td>
                            <td>{`${amount}$`}</td>
                        </tr>
                    </tbody>
                </table>
                <PaypalButton amount={amount} />
            </Dialog>
        )
    }

    renderTabs = () => {
        return (
            <Tabs>
                <TabList>
                    <Tab>New ({ this.state.uploadedAudio.length })</Tab>
                    <Tab>Processing ({ this.state.processingAudio.length })</Tab>
                    <Tab>Ready ({ this.state.finishedAudio.length })</Tab>
                </TabList>

                <TabPanel>
                    <DataTable value={this.state.uploadedAudio}>
                        <Column className={s['NameColumn']} field="fileName" header="Filename" />
                        <Column className={s['DurationColumn']} body={this.durationCell} field="duration" header="Duration" />
                        <Column className={s['CostColumn']} body={this.costCell} field="duration" header="Cost" />
                        <Column className={s['DurationColumn']} body={this.transcribeCell} field="fileName" />
                        <Column className={s['TrashColumn']} body={this.deleteCell} field="fileName" />
                    </DataTable>
                    {this.renderDeletionDialog()}
                    {this.renderCheckoutDialog()}
                </TabPanel>
                <TabPanel>
                    <DataTable value={this.state.processingAudio}>
                        <Column className={s['NameColumn']} field="fileName" header="Filename" />
                        <Column className={s['DurationColumn']} body={this.durationCell} field="duration" header="Duration" />
                        <Column className={s['DurationColumn']} body={this.progressCell} field="progress" header="Progress" />
                        <Column className={s['DurationColumn']} body={this.timeEstimatedFinishCell} field="timeEstimatedFinish" header="Estimated time" />
                    </DataTable>
                </TabPanel>
                <TabPanel>
                    <DataTable value={this.state.finishedAudio}>
                        <Column className={s['NameColumn']} body={this.nameLinkCell} field="fileName" header="Filename (click to get text)" />
                        <Column className={s['DurationColumn']} body={this.durationCell} field="duration" header="Duration" />
                        <Column className={s['TimeCreatedColumn']} body={this.timeCreatedCell} field="timeCreated" header="Download time" />
                    </DataTable>
                </TabPanel>
            </Tabs>
        )
    }

    render() {
        return (
            <div className={s.App}>
                <div className={s["HeaderWrapper"]}>
                    <header className={s["AppHeader"]}>
                        <div className={s["HeaderLeftSide"]}>
                            <i className="pi pi-file" style={{ fontSize: '1.7em', marginRight: '2px' }}></i>
                            <Switch>
                                <Route exact path={this.props.match.path}>
                                    <h4>Speech to text (transcribation)</h4>
                                </Route>
                                <Route
                                    path={`${this.props.match.path}/editor/:fileName`}
                                    render={({ match }) => (<h4>{match.params.fileName}</h4>)}
                                />
                            </Switch>
                        </div>
                        <div className={s["HeaderRightSide"]}>
                            <h5>{this.state.user.email}</h5>
                        </div>
                    </header>
                </div>
                <div className={s["ContentWrapper"]}>
                    <div className={s["AppContent"]}>
                        <div className={s["Panel"]}>
                            <Route path={`${this.props.match.path}/editor`}>
                                <Link to={this.props.match.path}>
                                    <Button
                                        label="Back"
                                        icon="pi pi-arrow-circle-up"
                                    />
                                </Link>
                            </Route>
                            <div className="marg-left-8">
                                <UploadButton uid={this.state.user.uid} />
                            </div>
                            <div className="marg-left-8">
                                <StartTranscribationButton uploadedAudio={this.state.uploadedAudio}/>
                            </div>
                        </div>
                        <div className={s["MainBlock"]}>
                            <Switch>
                                <Route exact path={this.props.match.path}>
                                    {this.renderTabs()}
                                </Route>
                                <Route
                                    path={`${this.props.match.path}/editor/:fileName`}
                                    component={Editor}
                                />
                            </Switch>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Account;
