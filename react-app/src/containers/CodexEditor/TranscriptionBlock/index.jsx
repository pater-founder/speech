import React from 'react';
import ReactDOM from 'react-dom';
import trimEnd from 'lodash/trimEnd';
import { Words } from './Words'

export default class Index {
    constructor({data, config = {}, api}) {
        this.api = api;
        this.data = data;
        this.audioRef = config.audioRef;
        this._CSS = {
            block: this.api.styles.block,
            wrapper: 'ce-paragraph'
        };
        this._element = this.drawView();
    }

    drawView() {
        const div = document.createElement('div');
        div.classList.add(this._CSS.wrapper, this._CSS.block);
        // div.contentEditable = true;
        console.log('data!:', this.data);
        ReactDOM.render(<Words data={this.data} audioRef={this.audioRef} />, div);
        return div;
    }

    render() {
        return this._element;
    }

    save(toolsContent) {
        return {
            ...this.data,
            words: Array
                .from(toolsContent.children[0].children[0].children)
                .map((span, idx) => ({
                    ...this.data.words[idx],
                    word: trimEnd(span.textContent)
                }))
        };
    }

    // merge(data) {
    //     let newData = {
    //         text : this.data.text + data.text
    //     };
    //
    //     this.data = newData;
    // }
    //
    // validate(savedData) {
    //     if (savedData.text.trim() === '') {
    //         return false;
    //     }
    //
    //     return true;
    // }

    // onPaste(event) {
    //     const data = {
    //         text: event.detail.data.innerHTML
    //     };
    //
    //     this.data = data;
    // }
    //
    // static get sanitize() {
    //     return {
    //         text: {
    //             br: true,
    //         }
    //     };
    // }

    // static get pasteConfig() {
    //     return {
    //         tags: [ 'P' ]
    //     };
    // }
}
