import React, { Component } from 'react';
// import floor from 'lodash/floor';
import toNumber from 'lodash/toNumber';
import isString from 'lodash/isString';
import trimEnd from 'lodash/trimEnd';
import s from './transcription-block.module.css';

export class Words extends Component {
    static defaultProps = {
        data: null,
        audioRef: null
    }
    state = {
        currentTime: 0
    };

    componentDidMount() {
        if (this.props.audioRef) {
            console.log('this.props.audioRef', this.props.audioRef);
            // this.props.audioRef.current.ontimeupdate = e => this.setState({ currentTime: e.target.currentTime});
            this.props.audioRef.current.addEventListener('timeupdate', e => this.setState({ currentTime: e.target.currentTime}));
        }
    }

    setTime = (time) => {
        console.log('end time:', time);
        this.props.audioRef.current.pause();
        this.props.audioRef.current.currentTime = time;
    }

    render() {
        const { words = [] } = this.props.data;
        // console.log('words:', words);
        return (
            <div contentEditable className="no-outline">
                <p className={s['Paragraph']}>
                    {words.map(({ word, endTime }, id) => {
                        // гугл то и дело меняет апи
                        let end;
                        if (isString(endTime)) {
                            end = toNumber(trimEnd(endTime, 's'));
                        } else {
                            end = toNumber(endTime.seconds) + endTime.nanos / 100000000000;
                        }
                        const isPlayed = this.state.currentTime >= end;
                        return (
                            <span
                                key={id}
                                className={[isPlayed ? s['Played'] : '']}
                                onClick={() => this.setTime(end)}
                            >{word} </span>
                        )
                    })}
                </p>
            </div>
        );
    }
}
