import React, { Component } from 'react';
import EditorJS from 'codex-editor';
import throttle from 'lodash/throttle';
import TranscriptionBlock from './TranscriptionBlock'
import './codex-editor.css';
import s from "./codex-editor.module.css";
import isEmpty from "lodash/isEmpty";

const storage = window.firebase.storage();
const db = window.firebase.firestore();
const auth = window.firebase.auth();
const storageRoot = storage.ref();

class CodexEditor extends Component {
    state = {
        results: [],
        selectedDoc: null,
        audioSrc: null
    }
    audioRef = React.createRef();
    editor = null;

    componentDidMount() {
        auth.onAuthStateChanged(async user => {
            const selectedDoc = db.collection('accounts')
                .doc(user.uid)
                .collection('transcriptions')
                .doc(decodeURI(this.props.match.params.fileName));
            const docSnap = await selectedDoc.get();
            const { results } = docSnap.data();
            this.setState({ results, selectedDoc });

            const transBlocks = results.map(result => {
                return {
                    type: "transcription",
                    data: result.alternatives[0]
                }
            });
            this.editor = new EditorJS({
                holderId: 'transcription-editor',
                autofocus: true,
                tools: {
                    transcription: {
                        class: TranscriptionBlock,
                        config: {
                            audioRef: this.audioRef
                        }
                        // inlineToolbar : true
                    }
                },
                data: {
                    time: Date.now(),
                    version: "2.12.4",
                    blocks: [
                        ...transBlocks
                    ]
                },
                onChange: throttle(this.saveTranscription, 3000, { leading: false }),
            })

            storageRoot
                .child(`accounts/${user.uid}/uploaded-audio/${decodeURI(this.props.match.params.fileName).replace(/\.[^/.]+$/, '')}_monoOutput.flac`)
                .getDownloadURL()
                .then(audioSrc => {
                    this.setState({ audioSrc });
                    this.audioRef.current.load()
                })

        });

    }

    saveTranscription = async () => {
        const outputData = await this.editor.save();
        const results = outputData.blocks.map(block => ({ alternatives: [ block.data ] }));
        await this.state.selectedDoc.update({ results });
    }

    render() {
        return (
            <div>.
                <div id="transcription-editor"></div>
                <div className={s["PlayerContainer"]}>
                    <audio ref={this.audioRef} controls preload="auto" style={{width: '100%', height: '48px'}} >
                        <source src={this.state.audioSrc} />
                    </audio>
                </div>
            </div>
        );
    }
}

export default CodexEditor;
