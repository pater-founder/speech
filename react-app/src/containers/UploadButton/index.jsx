import React, { Component } from 'react';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { DataScroller } from 'primereact/datascroller';
import {ProgressBar} from 'primereact/progressbar';
import Dropzone from 'react-dropzone';
import round from 'lodash/round'
import isEmpty from 'lodash/isEmpty';

import s from './upload-button.module.css';

const storage = window.firebase.storage();
const storageRoot = storage.ref();

const toFileSize = (downloaded = 0, size) => {
    if (size < 1024) {
        return `${downloaded}B / ${size}B`
    }
    if (size >= 1024 && size <= 1024 * 1024) {
        return `${round(downloaded / 1024, 2)}Kb / ${round(size / 1024, 2)}Kb`
    }
    return `${round(downloaded / (1024 * 1024), 2)}Mb / ${round(size / (1024 * 1024), 2)}Mb`
};

class UploadButton extends Component {
    static defaultProps = {
        uid: null
    }
    state = {
        dialogVisible: false,
        files: [],
        filesUploadedCount: 0
    }

    onSelectFiles = (files) => {
        console.log('files:', files);
        this.setState({ files });
        for (let i = 0; i < files.length; i++) {
            const uid = this.props.uid;
            const fileRef = storageRoot.child('accounts/' + uid + '/uploaded-audio/' + files[i].name);
            const metadata = {
                customMetadata: {
                    'ownerUid': uid
                }
            };
            fileRef.put(files[i], metadata).on('state_changed',  (snapshot) => {
                const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                files[i].bytesTransferred = snapshot.bytesTransferred;
                files[i].progress = round(progress, 2);
                this.setState({ files: [...files] })
            }, (err) => {
                files[i].error = 'Ошибка загрузки файла';
                this.setState({ files: [...files] });
                console.error('Ошибка загрузки файла', err)
            }, () => {
                this.setState(prevState => ({ filesUploadedCount: prevState.filesUploadedCount + 1 }));
            })
        }
    };

    uploadItemTemplate = (file) => (
        <div className={s['Item']}>
            <div className={s['ItemContainer']}>
                <i className="pi pi-volume-up" style={{ fontSize: '1.7em', marginRight: '4px' }}></i>
                <div className={s['ItemText']}>
                    <div className={s['ItemFileName']}>{file.name}</div>
                    <div className={s['ItemFileSize']}>
                        {toFileSize(file.bytesTransferred, file.size)}
                        {file.error && (<span style={{ color: 'red'}}> {file.error}</span>)}
                    </div>
                </div>
            </div>
            <ProgressBar style={{ height: '4px' }} value={file.progress} />
        </div>
    )

    render() {
        return (
            <div>
                <Button label="Upload files" icon="pi pi-upload" onClick={() => this.setState({dialogVisible: true})} />
                <Dialog
                    header={isEmpty(this.state.files) ? "Upload files" : `Upload files ${this.state.filesUploadedCount} / ${this.state.files.length}`}
                    visible={this.state.dialogVisible}
                    style={{width: '50vw', overflowY: 'scroll', maxHeight: '90vh'}}
                    modal={true}
                    onHide={() => this.setState({dialogVisible: false, files: [], filesUploadedCount: 0 })}
                >
                    {!isEmpty(this.state.files) ? (
                        <DataScroller
                            value={this.state.files}
                            rows={this.state.files.length}
                            itemTemplate={this.uploadItemTemplate}
                        />
                    ) : (
                        <Dropzone onDrop={this.onSelectFiles}>
                            {({getRootProps, getInputProps}) => (
                                <section>
                                    <div {...getRootProps()} style={{textAlign: 'center'}}>
                                        <input {...getInputProps()} accept="audio/*" />
                                        <i className="pi pi-cloud-upload" style={{ fontSize: '10em'}}></i>
                                        <p>Drag your file here or click here to upload</p>
                                        <p>
                                            Требование к файлам:
                                            длительность не более 480 минут, частота дискретизации не менее 16000 Гц,
                                            хорошо различимая на слух речь. Внимание: файлы с одинаковым именем перезаписываются!
                                            Убедитесь, что файл с таким именем не был загружен ранее!
                                        </p>
                                    </div>
                                </section>
                            )}
                        </Dropzone>
                    )}
                </Dialog>
            </div>
            )
    }
}

export default UploadButton;
