import React, { Component } from 'react';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { durationToTime } from 'utils/time'
import {DataTable} from "primereact/datatable";
import {Column} from "primereact/column";
import s from "./index.module.css";

const functions = window.firebase.app().functions();
const transcribe = functions.httpsCallable('transcribe');

class UploadButton extends Component {
    static defaultProps = {
        uploadedAudio: []
    }
    state = {
        dialogVisible: false
    }

    onTranscriptButtonClick = async () => {
        await transcribe();
        return console.log('transcribe success');
    };

    durationCell = ({ duration }) => {
        return <span>{durationToTime(duration)}</span>
    }
    costCell = ({ duration }) => {
        return duration
    }

    render() {
        return (
            <div>
                <Button
                    label="Start transcribation"
                    icon="pi pi-file"
                    disabled={!this.props.uploadedAudio.length}
                    // onClick={() => this.setState({dialogVisible: true})}
                    onClick={this.onTranscriptButtonClick}
                />
                <Dialog
                    header="Оплата транскрибации"
                    visible={this.state.dialogVisible}
                    style={{width: '50vw'}}
                    modal={true}
                    onHide={() => this.setState({dialogVisible: false })}
                >
                    <DataTable value={this.props.uploadedAudio} style={{overflowY: 'scroll', maxHeight: '50vh'}}>
                        <Column className={s['NameColumn']} field="fileName" header="Название" />
                        <Column className={s['DurationColumn']} body={this.durationCell} field="duration" header="Длительность" />
                        <Column className={s['CostColumn']} body={this.costCell} field="duration" header="Стоимость" />
                    </DataTable>
                </Dialog>
            </div>
            )
    }
}

export default UploadButton;
