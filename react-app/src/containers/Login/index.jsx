import React, { Component } from 'react';
import './index.css';

class Index extends Component {
    componentDidMount() {
        const uiConfig = {
            signInSuccessUrl: '/account',
            signInOptions: [
                {
                    provider: window.firebase.auth.EmailAuthProvider.PROVIDER_ID,
                    requireDisplayName: false
                }
            ],
            credentialHelper: window.firebaseui.auth.CredentialHelper.NONE,
            tosUrl: 'https://my-project-1506630871701.firebaseapp.com/'
        };

        const ui = new window.firebaseui.auth.AuthUI(window.firebase.auth());
        ui.start('#firebaseui-auth-container', uiConfig);
    }

    render() {
        return (
            <div className="overlay">
                <h1>Speech to text transcription service</h1>
                <div id="firebaseui-auth-container"></div>
            </div>
        );
    }
}

export default Index;
