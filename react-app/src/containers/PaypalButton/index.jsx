import React, { Component } from 'react';

const paypal = window.paypal;

class PaypalButton extends Component {
    componentDidMount() {
        paypal.Buttons({
            createOrder: function(data, actions) {
                // This function sets up the details of the transaction, including the amount and line item details.
                return actions.order.create({
                    purchase_units: [{
                        amount: {
                            value: '1.01'
                        }
                    }]
                });
            },
        }).render('#paypal')
    }

    render() {
        return (
            <div id="paypal">
            </div>
        );
    }
}

export default PaypalButton;
