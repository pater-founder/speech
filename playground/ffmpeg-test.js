'use strict';

const ffmpeg = require('fluent-ffmpeg');
const ffprobe_static = require('ffprobe-static');
const path = require('path');
const fs = require('fs');

// if (fs.existsSync('/path/to/file.avi')) {
//     console.log('1')
// }
// if (fs.existsSync(path.resolve('./playground/mono.flac'))) {
//     console.log(path.resolve('./playground/mono.flac'))
// }
// if (fs.existsSync('mono.flac')) {
//     console.log('3')
// }

ffmpeg(path.resolve('./playground/file.wma'))
    .setFfprobePath(ffprobe_static.path)
    .ffprobe(function(err, metadata) {
        if (err) {
            console.log('error:', err);
        }
        console.dir(metadata);
        console.log('duration:', metadata.format.duration)
    });
