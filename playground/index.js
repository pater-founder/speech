// Imports the Google Cloud client library
const fs = require('fs');
const speech = require('@google-cloud/speech');

// set GOOGLE_APPLICATION_CREDENTIALS=C:\JSprojects\Speech\speech-transcriptor-19d44952ecf8.json
// обрати внимание без кавычек
"use strict";
// Instantiates a client
const client = new speech.SpeechClient();

// The path to the local file on which to perform speech recognition, e.g. /path/to/audio.raw
// const filename = '/path/to/audio.raw';

// The encoding of the audio file, e.g. 'LINEAR16'
// const encoding = 'LINEAR16';

// The sample rate of the audio file in hertz, e.g. 16000
// const sampleRateHertz = 16000;

// The BCP-47 language code to use, e.g. 'en-US'
// const languageCode = 'en-US';

const config = {
    encoding: 'FLAC',
    // sampleRateHertz: 44100,
    languageCode: 'ru-RU',
    enableWordTimeOffsets: true
};
const audio = {
    content: fs.readFileSync('mono.flac').toString('base64')
};

const request = {
    config: config,
    audio: audio
};

// Detects speech in the audio file
client.recognize(request).then((data) => {
    console.log(data);
    const response = data[0];
    const transcription = response.results.map(result => {
            console.log(result.alternatives[0].transcript + '\n+++');
            console.log(result.alternatives[0].words);
            return result.alternatives[0].transcript
    }).join('/n');
    console.log(`Transcription: `, transcription);
})
.catch((err) => {
    console.error('ERROR:', err);
});