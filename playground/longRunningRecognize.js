// Imports the Google Cloud client library
const fs = require('fs');
const speech = require('@google-cloud/speech');

async function asyncRecognize() {
    const client = new speech.SpeechClient();

    const config = {
        encoding: 'FLAC',
        sampleRateHertz: 8000,
        languageCode: 'ru-RU'
    };
    const audio = {
        // content: fs.readFileSync('mono.flac').toString('base64'),
        uri: `gs://speech-transcriptor.appspot.com/accounts/UZcTkm4hDhMhzZBRMR9zx594EqD3/uploaded-audio/7AB4C3D6 B028EDEC EB00FC73 784153DD_monoOutput.flac`
    };

    const request = {
        config: config,
        audio: audio
    };

// Detects speech in the audio file
    const [operation] = await client.longRunningRecognize(request);
    console.log('opearation:', operation);

// Get a Promise representation of the final result of the job
    const [response] = await operation.promise();
    const transcription = response.results
        .map(result => result.alternatives[0].transcript)
        .join('\n');
    console.log(`Transcription: ${transcription}`);
}

asyncRecognize();
